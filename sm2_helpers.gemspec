
lib = File.expand_path("../lib", __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require "sm2_helpers/version"

Gem::Specification.new do |spec|
  spec.name          = "sm2_helpers"
  spec.version       = Sm2Helpers::VERSION
  spec.authors       = ["Vadim Sergeevich"]
  spec.email         = ["vs.sidhart@gmail.com"]
  spec.summary       = 'sm2 helpers'
  spec.description   = 'helpers'
  spec.homepage      = "https://gitlab.com/sm2ai/sm2_helpers"
  spec.license       = "MIT"

  spec.files         = `git ls-files`.split("\n")
  spec.test_files    = `git ls-files -- spec/*`.split("\n")
  spec.require_paths = %w[lib config]

  spec.add_dependency "rails-i18n", "~> 6.0"

  spec.add_development_dependency "bundler", "~> 1.17"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "rspec", "~> 3.0"
end
