require "sm2_helpers/version"
require 'rails-i18n'
require 'action_view'
I18n.load_path += Dir[File.expand_path("../../config/locales/*.yml", __FILE__)]

I18n.available_locales = [:en, :ru]

module Sm2Helpers
  include ActiveSupport::NumberHelper
  include ActionView::Helpers::NumberHelper

  def number_human(num)
    return unless num

    number_with_precision(num, precision: 2, delimiter: ' ')
  end

  def num_with_curr(num, curr)
    return unless num

    number_to_currency(num, unit: curr || '')
  end

  def num_with_curr_sign(num, curr)
    return unless num

    number_to_currency(num, unit: currency_sign(curr) || '')
  end

  def num_with_curr_sign_and_suffix(num, curr, suffix)
    return unless num

    case I18n.locale
    when :ru
      "#{number_human(num)} #{suffix} #{currency_sign(curr)}"
    else
      "#{num_with_curr_sign(num, curr)} #{suffix}"
    end
  end

  def num_with_perc(num)
    return unless num

    [number_with_precision(num, precision: 2), '%'].join
  end

  def number_with_m2(num)
    return if num.nil? || num.zero?

    number_with_precision(num, precision: 2) + ' ' + I18n.t('sm2_helpers.square_meter')
  end

  def round_number_with_m2(num)
    return if num.nil? || num.zero?

    number_with_precision(num.round, precision: 2) + ' ' + I18n.t('sm2_helpers.square_meter')
  end

  def number_with_ha(num)
    return if num.nil? || num.zero?

    number_with_precision(num, precision: 2) + ' ' + I18n.t('sm2_helpers.ha')
  end

  def delimited_number_with_m2(num)
    return unless num

    number_with_precision(num, precision: 2, delimiter: ' ') + ' ' + I18n.t('sm2_helpers.square_meter')
  end

  def number_to_curr_per_m2(num, curr)
    return unless num
    return I18n.t('sm2_helpers.na') if num.zero?

    number_to_currency(num, unit: currency_sign(curr) || '') + '/' + I18n.t('sm2_helpers.square_meter')
  end

  def deal_amount_format(value, curr)
    return I18n.t('sm2_helpers.no_data') if value.to_i.zero?

    trunc_value = number_with_precision(value, precision: 2, strip_insignificant_zeros: true)
    locale_specialty_format(trunc_value, curr)
  end

  def locale_specialty_format(value, curr)
    I18n.locale == :en ? en_currency_with_m2(value, curr) : ru_currency_with_m2(value, curr)
  end

  def en_currency_with_m2(value, curr)
    '~' + number_to_currency(value.to_s, unit: currency_sign(curr) || '', format: '%u %n') + ' /' + I18n.t('sm2_helpers.square_meter')
  end

  def ru_currency_with_m2(value, curr)
    number_to_currency('~ ' + value.to_s, unit: currency_sign(curr) || '') + '/' + I18n.t('sm2_helpers.square_meter')
  end

  def currency_sign(currency_name)
    return unless currency_name

    case currency_name.to_sym
    when :usd then '$'
    when :rub then '₽'
    when :eur then '€'
    when :gbp then '£'
    else ''
    end
  end

  def human_range_with_currency(min, max, currency)
    if min == max || min.blank? || max.blank?
      value = min.presence || max.presence
      "#{number_to_currency(value, unit: '').strip} #{currency_sign(currency)}/#{t('custom.square_meter')}"
    else
      "#{number_to_currency(min, unit: '').strip} – #{number_to_currency(max, unit: '').strip} #{currency_sign(currency)}/#{t('custom.square_meter')}"
    end
  end
end
